This repository contains a jupyter notebook accompanying the manuscript "Dynamic microfluidic single-cell screening identifies pheno-tuning compounds to potentiate tuberculosis therapy" by Mistretta et al.

The notebook uses [`omnipose`](https://github.com/kevinjohncutler/omnipose) to segment bacteria from phase contrast microscopy and measure fluorescent signals using [scikit-image](https://github.com/scikit-image/scikit-image). For segmentation, a custom omnipose model has been trained using a training dataset combining the dataset available from omnipose and additional manually annotated images. The resulting model can be found under ./models.

To run the notebook locally, download the repository and follow the installation instructions detailed in the notebook.

*Questions/feedback to Marvin Albert @ Image Analysis Hub @ Institut Pasteur (marvin.albert@pasteur.fr)*